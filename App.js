import React from 'react';
import {Button, View, KeyboardAvoidingView,Platform} from 'react-native';
import Text from './component/Text';
import Input from './component/Input';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';

const InputScreen = () => {
  return <Input />;
};

const TextScreen = () => {
  return <Text />;
};

const MainScreen = ({navigation}) => {
  return (
    <View style={{alignItems: 'center'}}>
      <View style={{marginTop: 10}}>
        <Button
          title="Input Demo"
          onPress={() => {
            navigation.navigate('Input');
          }}
        />
      </View>
      <View style={{marginTop: 10}}>
        <Button
          title="Text Demo"
          onPress={() => {
            navigation.navigate('Text');
          }}
        />
      </View>
    </View>
  );
};

function App() {
  return (
    <View style={{flex: 1}}>
      <NavigationContainer>
        <Stack.Navigator>
          <Stack.Screen name="Main" component={MainScreen} />
          <Stack.Screen name="Input" component={InputScreen} />
          <Stack.Screen name="Text" component={TextScreen} />
        </Stack.Navigator>
      </NavigationContainer>
      
        
      
    </View>
  );
}

const Stack = createNativeStackNavigator();

export default App;
