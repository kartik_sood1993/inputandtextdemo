import React, {useState} from 'react';
import {View, ScrollView, KeyboardAvoidingView, Platform} from 'react-native';
import {TextInput, DefaultTheme, HelperText} from 'react-native-paper';
import {SafeAreaView} from 'react-native-safe-area-context';

export default Input => {
  const [phoneNo, setPhoneNo] = useState('');
  const [email, setEmail] = React.useState('');
  const [emailCustom, setEmailCustom] = React.useState('');

  const hasErrors = () => {
    return !email.includes('@');
  };

  const hasErrorsNew = () => {
    return !emailCustom.includes('@');
  };

  return (
    <SafeAreaView>
      <KeyboardAvoidingView
        behavior={Platform.OS === 'ios' ? 'padding' : 'height'}>
        <ScrollView style={{backgroundColor: 'white'}}>
          <View
            style={{
              marginHorizontal: 20,
              marginVertical: 10,
            }}>
            {/* without mode (default: flat)*/}
            <TextInput
              label="Email"
              placeholder="Enter your email address"
              style={{backgroundColor: 'white'}}
            />

            {/* with flat mode*/}
            <TextInput style={{marginTop: 10}} label="FirstName" mode="flat" />

            <View
              style={{
                flexDirection: 'row',
                width: '100%',
                alignItems: 'center',
              }}>
              {/* with outlined mode*/}
              <TextInput
                style={{marginTop: 10, width: '50%'}}
                label="lastName"
                mode="outlined"
                placeholder="Enter your last name"
              />
              <TextInput
                style={{marginTop: 10, width: '50%'}}
                label="lastName"
                mode="outlined"
                placeholder="Enter your last name"
              />
            </View>

            {/* with secure entry (password)*/}
            <TextInput
              style={{marginTop: 10}}
              label="Password"
              secursadasdeTextEntry={true}
            />

            {/* on Change text and data using useState hook*/}
            <TextInput
              style={{marginTop: 10}}
              label="phoneNumber"
              keyboardType={'numeric'}
              onChangeText={text => setPhoneNo(text)}
            />

            {/* with disabled mode*/}
            <TextInput
              style={{marginTop: 10}}
              label="Disabled"
              mode="outlined"
              disabled={true}
            />

            {/* with error style*/}
            <TextInput
              style={{marginTop: 10}}
              mode="outlined"
              label="Email"
              value={email}
              onChangeText={text => setEmail(text)}
              error={hasErrors()}
            />
            <HelperText type="error" visible={hasErrors()}>
              Email address is invalid!
            </HelperText>

            {/* with error style with custom color*/}
            <TextInput
              style={{marginTop: 10}}
              error={hasErrorsNew()}
              label="Email"
              mode="outlined"
              onChangeText={text => setEmailCustom(text)}
              theme={{
                ...DefaultTheme,
                colors: {
                  error: 'blue',
                },
              }}
            />
            <HelperText
              type="error"
              visible={hasErrorsNew()}
              theme={{
                ...DefaultTheme,
                colors: {
                  error: 'blue',
                },
              }}>
              Email address is invalid!
            </HelperText>

            {/* with color changes  and multiline text*/}
            <TextInput
              style={{marginTop: 10}}
              mode="outlined"
              placeholder="Enter your addess"
              selectionColor="orange"
              multiline={true}
              numberOfLines={6}
              outlineColor="teal"
              editable={false}
            />

            {/* With Icon*/}
            <TextInput
              style={{marginTop: 10}}
              mode="outlined"
              keyboardType="email-address"
              left={<TextInput.Icon name="home" />}
              right={<TextInput.Icon name="camera" />}
            />
          </View>
        </ScrollView>
      </KeyboardAvoidingView>
    </SafeAreaView>
  );
};
