import React from 'react';
import {
  View,
  Text as TextView,
  StyleSheet,
  TouchableOpacity,
  ToastAndroid,
  Platform,
  AlertIOS,
  Alert,
} from 'react-native';

import {
  Title,
  Subheading,
  Paragraph,
  Headline,
  Caption,
} from 'react-native-paper';

export default Text => {
  const notifyMessage = (msg) => {
    if (Platform.OS === 'android') {
      ToastAndroid.show(msg, ToastAndroid.SHORT);
    } else {
      AlertIOS.alert(msg);
    }
  };

  return (
    <View style={{marginHorizontal: 8}}>
      <Title
        style={{
          backgroundColor: 'red',
          color: 'yellow',
          borderColor: 'blue',
          borderWidth: 1,
          padding: 8,
        }}>
        Title Demo
      </Title>
      <Subheading>Sub heading</Subheading>
      <Paragraph numberOfLines={2}>
        This is a very long text {'\n '}
         that will overflow on a small device, so we need to truncate it fdkjfklj klj klafjdkljskl jgfkldsj klgjkldsj gkljskdl jgklsjfkl gjklfsj klgjfdklgj kljfsgkl jkl
      </Paragraph>
      <Headline>Headline</Headline>
      <Caption>Caption</Caption>

      {/* Custom font */}
      <TextView
        numberOfLines={1}
        ellipsizeMode="tail"
        style={{fontFamily: 'SF-UI-Display-', fontSize: 16}}>
        This is a very long text that will overflow on a small device, so we
        need to truncate it
      </TextView>

      {/* Inner Text + underline*/}
      <TextView numberOfLines={3} ellipsizeMode="tail" style={{fontSize: 17}}>
        This is a very long text that will overflow on a small device,{'\n '}
        <TouchableOpacity
          activeOpacity={0.8}
          onPress={() => notifyMessage('Item clicked')}>
          
          <TextView style={styles.underline}>
            https://google.com
          </TextView>
        </TouchableOpacity>
      </TextView>
    </View>
  );
};

const styles = StyleSheet.create({
  bold: {fontWeight: 'bold', color: 'blue'},
  italic: {fontStyle: 'italic', color: 'blue'},
  underline: {textDecorationLine: 'underline', color: 'blue'},
});
